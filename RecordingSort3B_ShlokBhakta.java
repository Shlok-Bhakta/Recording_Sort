/*
Shlok Bhakta
9/30/21
recording stuff yay
*/
package recordingsort3b_shlokbhakta;
import javax.swing.*;
import java.util.*;
public class RecordingSort3B_ShlokBhakta {

	public static void main(String[] args) {
		/*
		r1 [0][0] = song title | [1][0] = artist | [2][0] = time
		r2 [0][1] = song title | [1][1] = artist | [2][1] = time
		r3 [0][2] = song title | [1][2] = artist | [2][2] = time
		r4 [0][3] = song title | [1][3] = artist | [2][3] = time
		r5 [0][4] = song title | [1][4] = artist | [2][4] = time
		*/
		
		JCheckBox descending = new JCheckBox("Descending Order");
		String[][] records = new String[3][5];
		String[][] newrecords = new String[3][5];
		Object[] sortOptions = {"Song", "Artist", "Length", descending};
		boolean tick = false;
			int l1 = -1;
			int l2 = -1;			
			int l3 = -1;			
			int l4 = -1;			
			int l5 = -1;

		
		int j = JOptionPane.showConfirmDialog(null, "would you like to use some premade records?");
		
		if(j == 0){
			//premade 1
			Recording3B.setSong("Sad");
			Recording3B.setArtist("JollyJim");
			Recording3B.setTime("2.43");
			
			records[0][0] = Recording3B.getSong();
			records[1][0] = Recording3B.getArtist();
			records[2][0] = Recording3B.getTime();
			
			//premade 2
			Recording3B.setSong("UP");
			Recording3B.setArtist("Downer");
			Recording3B.setTime("1.23");
			
			records[0][1] = Recording3B.getSong();
			records[1][1] = Recording3B.getArtist();
			records[2][1] = Recording3B.getTime();			
			
			//premade 3
			Recording3B.setSong("Happy");
			Recording3B.setArtist("DepressionMan");
			Recording3B.setTime("3.52");
			
			records[0][2] = Recording3B.getSong();
			records[1][2] = Recording3B.getArtist();
			records[2][2] = Recording3B.getTime();			
				
			//premade 4
			Recording3B.setSong("Inside");
			Recording3B.setArtist("Outerland");
			Recording3B.setTime("2.14");
			
			records[0][3] = Recording3B.getSong();
			records[1][3] = Recording3B.getArtist();
			records[2][3] = Recording3B.getTime();			

			//premade 5
			Recording3B.setSong("Hit");
			Recording3B.setArtist("FluffyPillow Productions");
			Recording3B.setTime("0.52");
			
			records[0][4] = Recording3B.getSong();
			records[1][4] = Recording3B.getArtist();
			records[2][4] = Recording3B.getTime();			
			
		}else{
			//usermade 1
			Recording3B.setSong(JOptionPane.showInputDialog(null, "name a record/song to store/sort.", "Record/Song Name 1", JOptionPane.QUESTION_MESSAGE));
			Recording3B.setArtist(JOptionPane.showInputDialog(null, "Who is the artist of the record/song?", "Artist 1", JOptionPane.QUESTION_MESSAGE));
			Recording3B.setTime(JOptionPane.showInputDialog(null, "How long is the record/song?", "Length 1", JOptionPane.QUESTION_MESSAGE));
			
			records[0][0] = Recording3B.getSong();
			records[1][0] = Recording3B.getArtist();
			records[2][0] = Recording3B.getTime();			
			
			//usermade 2
			Recording3B.setSong(JOptionPane.showInputDialog(null, "name a record/song to store/sort.", "Record/Song Name 2", JOptionPane.QUESTION_MESSAGE));
			Recording3B.setArtist(JOptionPane.showInputDialog(null, "Who is the artist of the record/song?", "Artist 2", JOptionPane.QUESTION_MESSAGE));
			Recording3B.setTime(JOptionPane.showInputDialog(null, "How long is the record/song?", "Length 2", JOptionPane.QUESTION_MESSAGE));
			
			records[0][1] = Recording3B.getSong();
			records[1][1] = Recording3B.getArtist();
			records[2][1] = Recording3B.getTime();		
			
			//usermade 3
			Recording3B.setSong(JOptionPane.showInputDialog(null, "name a record/song to store/sort.", "Record/Song Name 3", JOptionPane.QUESTION_MESSAGE));
			Recording3B.setArtist(JOptionPane.showInputDialog(null, "Who is the artist of the record/song?", "Artist 3", JOptionPane.QUESTION_MESSAGE));
			Recording3B.setTime(JOptionPane.showInputDialog(null, "How long is the record/song?", "Length 3", JOptionPane.QUESTION_MESSAGE));
			
			records[0][2] = Recording3B.getSong();
			records[1][2] = Recording3B.getArtist();
			records[2][2] = Recording3B.getTime();			
			
			//usermade 4
			Recording3B.setSong(JOptionPane.showInputDialog(null, "name a record/song to store/sort.", "Record/Song Name 4", JOptionPane.QUESTION_MESSAGE));
			Recording3B.setArtist(JOptionPane.showInputDialog(null, "Who is the artist of the record/song?", "Artist 4", JOptionPane.QUESTION_MESSAGE));
			Recording3B.setTime(JOptionPane.showInputDialog(null, "How long is the record/song?", "Length 4", JOptionPane.QUESTION_MESSAGE));
			
			records[0][3] = Recording3B.getSong();
			records[1][3] = Recording3B.getArtist();
			records[2][3] = Recording3B.getTime();	
			
			//usermade 5
			Recording3B.setSong(JOptionPane.showInputDialog(null, "name a record/song to store/sort.", "Record/Song Name 5", JOptionPane.QUESTION_MESSAGE));
			Recording3B.setArtist(JOptionPane.showInputDialog(null, "Who is the artist of the record/song?", "Artist 5", JOptionPane.QUESTION_MESSAGE));
			Recording3B.setTime(JOptionPane.showInputDialog(null, "How long is the record/song?", "Length 5", JOptionPane.QUESTION_MESSAGE));
			
			records[0][4] = Recording3B.getSong();
			records[1][4] = Recording3B.getArtist();
			records[2][4] = Recording3B.getTime();	
			

		}
		
		int u = JOptionPane.showOptionDialog(null, "How would you like to sort the Records/Songs", "Sort Choice", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, sortOptions, sortOptions[0]);
		
		if(descending.isSelected()){
		tick = true;
		}else{
			tick = false;
		}
		
		
		if(u == 0 && tick == true){
			String[] songs = {records[0][0], records[0][1], records[0][2], records[0][3], records[0][4]};
			String[] preSongs = {records[0][0], records[0][1], records[0][2], records[0][3], records[0][4]};
			Arrays.sort(songs);
			
			Collections.reverse(Arrays.asList(songs));
			
			
			//songs1
			if(songs[0] == preSongs[0]){
				l1 = 0;
			}
			if(songs[0] == preSongs[1]){
				l1 = 1;
			}
			if(songs[0] == preSongs[2]){
				l1 = 2;
			}
			if(songs[0] == preSongs[3]){
				l1 = 3;
			}
			if(songs[0] == preSongs[4]){
				l1 = 4;
			}
			
			
			
			//songs2
			if(songs[1] == preSongs[0]){
				l2 = 0;
			}
			if(songs[1] == preSongs[1]){
				l2 = 1;
			}
			if(songs[1] == preSongs[2]){
				l2 = 2;
			}
			if(songs[1] == preSongs[3]){
				l2 = 3;
			}
			if(songs[1] == preSongs[4]){
				l2 = 4;
			}
			
			//songs3
			if(songs[2] == preSongs[0]){
				l3 = 0;
			}
			if(songs[2] == preSongs[1]){
				l3 = 1;
			}
			if(songs[2] == preSongs[2]){
				l3 = 2;
			}
			if(songs[2] == preSongs[3]){
				l3 = 3;
			}
			if(songs[2] == preSongs[4]){
				l3 = 4;
			}			



			//songs4
			if(songs[3] == preSongs[0]){
				l4 = 0;
			}
			if(songs[3] == preSongs[1]){
				l4 = 1;
			}
			if(songs[3] == preSongs[2]){
				l4 = 2;
			}
			if(songs[3] == preSongs[3]){
				l4 = 3;
			}
			if(songs[3] == preSongs[4]){
				l4 = 4;
			}			


			//songs5
			if(songs[4] == preSongs[0]){
				l5 = 0;
			}
			if(songs[4] == preSongs[1]){
				l5 = 1;
			}
			if(songs[4] == preSongs[2]){
				l5 = 2;
			}
			if(songs[4] == preSongs[3]){
				l5 = 3;
			}
			if(songs[4] == preSongs[4]){
				l5 = 4;
			}
			
		//restructuring
		newrecords[0][0] = records[0][l1];
		newrecords[0][1] = records[0][l2];
		newrecords[0][2] = records[0][l3];
		newrecords[0][3] = records[0][l4];
		newrecords[0][4] = records[0][l5];
		
		newrecords[1][0] = records[1][l1];
		newrecords[1][1] = records[1][l2];
		newrecords[1][2] = records[1][l3];
		newrecords[1][3] = records[1][l4];
		newrecords[1][4] = records[1][l5];
		
		newrecords[2][0] = records[2][l1];
		newrecords[2][1] = records[2][l2];
		newrecords[2][2] = records[2][l3];
		newrecords[2][3] = records[2][l4];
		newrecords[2][4] = records[2][l5];
		}
		
		if(u == 1 && tick == true){
			String[] art = {records[1][0], records[1][1], records[1][2], records[1][3], records[1][4]};
			String[] preArt = {records[1][0], records[1][1], records[1][2], records[1][3], records[1][4]};
			Arrays.sort(art);

			Collections.reverse(Arrays.asList(art));
			
			//art1
			if(art[0] == preArt[0]){
				l1 = 0;
			}
			if(art[0] == preArt[1]){
				l1 = 1;
			}
			if(art[0] == preArt[2]){
				l1 = 2;
			}
			if(art[0] == preArt[3]){
				l1 = 3;
			}
			if(art[0] == preArt[4]){
				l1 = 4;
			}
			
			
			
			//art2
			if(art[1] == preArt[0]){
				l2 = 0;
			}
			if(art[1] == preArt[1]){
				l2 = 1;
			}
			if(art[1] == preArt[2]){
				l2 = 2;
			}
			if(art[1] == preArt[3]){
				l2 = 3;
			}
			if(art[1] == preArt[4]){
				l2 = 4;
			}
			
			//art3
			if(art[2] == preArt[0]){
				l3 = 0;
			}
			if(art[2] == preArt[1]){
				l3 = 1;
			}
			if(art[2] == preArt[2]){
				l3 = 2;
			}
			if(art[2] == preArt[3]){
				l3 = 3;
			}
			if(art[2] == preArt[4]){
				l3 = 4;
			}			



			//art4
			if(art[3] == preArt[0]){
				l4 = 0;
			}
			if(art[3] == preArt[1]){
				l4 = 1;
			}
			if(art[3] == preArt[2]){
				l4 = 2;
			}
			if(art[3] == preArt[3]){
				l4 = 3;
			}
			if(art[3] == preArt[4]){
				l4 = 4;
			}			


			//art5
			if(art[4] == preArt[0]){
				l5 = 0;
			}
			if(art[4] == preArt[1]){
				l5 = 1;
			}
			if(art[4] == preArt[2]){
				l5 = 2;
			}
			if(art[4] == preArt[3]){
				l5 = 3;
			}
			if(art[4] == preArt[4]){
				l5 = 4;
			}
			
		//restructuring
		newrecords[0][0] = records[0][l1];
		newrecords[0][1] = records[0][l2];
		newrecords[0][2] = records[0][l3];
		newrecords[0][3] = records[0][l4];
		newrecords[0][4] = records[0][l5];
		
		newrecords[1][0] = records[1][l1];
		newrecords[1][1] = records[1][l2];
		newrecords[1][2] = records[1][l3];
		newrecords[1][3] = records[1][l4];
		newrecords[1][4] = records[1][l5];
		
		newrecords[2][0] = records[2][l1];
		newrecords[2][1] = records[2][l2];
		newrecords[2][2] = records[2][l3];
		newrecords[2][3] = records[2][l4];
		newrecords[2][4] = records[2][l5];
		
		}
		
		if(u == 2 && tick == true){
			String[] time = {records[2][0], records[2][1], records[2][2], records[2][3], records[2][4]};
			String[] preTime = {records[2][0], records[2][1], records[2][2], records[2][3], records[2][4]};
			Arrays.sort(time);

			Collections.reverse(Arrays.asList(time));
			
			//time1
			if(time[0] == preTime[0]){
				l1 = 0;
			}
			if(time[0] == preTime[1]){
				l1 = 1;
			}
			if(time[0] == preTime[2]){
				l1 = 2;
			}
			if(time[0] == preTime[3]){
				l1 = 3;
			}
			if(time[0] == preTime[4]){
				l1 = 4;
			}
			
			
			
			//time2
			if(time[1] == preTime[0]){
				l2 = 0;
			}
			if(time[1] == preTime[1]){
				l2 = 1;
			}
			if(time[1] == preTime[2]){
				l2 = 2;
			}
			if(time[1] == preTime[3]){
				l2 = 3;
			}
			if(time[1] == preTime[4]){
				l2 = 4;
			}
			
			//time3
			if(time[2] == preTime[0]){
				l3 = 0;
			}
			if(time[2] == preTime[1]){
				l3 = 1;
			}
			if(time[2] == preTime[2]){
				l3 = 2;
			}
			if(time[2] == preTime[3]){
				l3 = 3;
			}
			if(time[2] == preTime[4]){
				l3 = 4;
			}			



			//time4
			if(time[3] == preTime[0]){
				l4 = 0;
			}
			if(time[3] == preTime[1]){
				l4 = 1;
			}
			if(time[3] == preTime[2]){
				l4 = 2;
			}
			if(time[3] == preTime[3]){
				l4 = 3;
			}
			if(time[3] == preTime[4]){
				l4 = 4;
			}			


			//time5
			if(time[4] == preTime[0]){
				l5 = 0;
			}
			if(time[4] == preTime[1]){
				l5 = 1;
			}
			if(time[4] == preTime[2]){
				l5 = 2;
			}
			if(time[4] == preTime[3]){
				l5 = 3;
			}
			if(time[4] == preTime[4]){
				l5 = 4;
			}
			
		//restructuring
		newrecords[0][0] = records[0][l1];
		newrecords[0][1] = records[0][l2];
		newrecords[0][2] = records[0][l3];
		newrecords[0][3] = records[0][l4];
		newrecords[0][4] = records[0][l5];
		
		newrecords[1][0] = records[1][l1];
		newrecords[1][1] = records[1][l2];
		newrecords[1][2] = records[1][l3];
		newrecords[1][3] = records[1][l4];
		newrecords[1][4] = records[1][l5];
		
		newrecords[2][0] = records[2][l1];
		newrecords[2][1] = records[2][l2];
		newrecords[2][2] = records[2][l3];
		newrecords[2][3] = records[2][l4];
		newrecords[2][4] = records[2][l5];
		
		}
		
		if(u == 0 && tick == false){
			String[] songs = {records[0][0], records[0][1], records[0][2], records[0][3], records[0][4]};
			String[] preSongs = {records[0][0], records[0][1], records[0][2], records[0][3], records[0][4]};
			Arrays.sort(songs);

			
			
			//songs1
			if(songs[0] == preSongs[0]){
				l1 = 0;
			}
			if(songs[0] == preSongs[1]){
				l1 = 1;
			}
			if(songs[0] == preSongs[2]){
				l1 = 2;
			}
			if(songs[0] == preSongs[3]){
				l1 = 3;
			}
			if(songs[0] == preSongs[4]){
				l1 = 4;
			}
			
			
			
			//songs2
			if(songs[1] == preSongs[0]){
				l2 = 0;
			}
			if(songs[1] == preSongs[1]){
				l2 = 1;
			}
			if(songs[1] == preSongs[2]){
				l2 = 2;
			}
			if(songs[1] == preSongs[3]){
				l2 = 3;
			}
			if(songs[1] == preSongs[4]){
				l2 = 4;
			}
			
			//songs3
			if(songs[2] == preSongs[0]){
				l3 = 0;
			}
			if(songs[2] == preSongs[1]){
				l3 = 1;
			}
			if(songs[2] == preSongs[2]){
				l3 = 2;
			}
			if(songs[2] == preSongs[3]){
				l3 = 3;
			}
			if(songs[2] == preSongs[4]){
				l3 = 4;
			}			



			//songs4
			if(songs[3] == preSongs[0]){
				l4 = 0;
			}
			if(songs[3] == preSongs[1]){
				l4 = 1;
			}
			if(songs[3] == preSongs[2]){
				l4 = 2;
			}
			if(songs[3] == preSongs[3]){
				l4 = 3;
			}
			if(songs[3] == preSongs[4]){
				l4 = 4;
			}			


			//songs5
			if(songs[4] == preSongs[0]){
				l5 = 0;
			}
			if(songs[4] == preSongs[1]){
				l5 = 1;
			}
			if(songs[4] == preSongs[2]){
				l5 = 2;
			}
			if(songs[4] == preSongs[3]){
				l5 = 3;
			}
			if(songs[4] == preSongs[4]){
				l5 = 4;
			}
			
		//restructuring
		newrecords[0][0] = records[0][l1];
		newrecords[0][1] = records[0][l2];
		newrecords[0][2] = records[0][l3];
		newrecords[0][3] = records[0][l4];
		newrecords[0][4] = records[0][l5];
		
		newrecords[1][0] = records[1][l1];
		newrecords[1][1] = records[1][l2];
		newrecords[1][2] = records[1][l3];
		newrecords[1][3] = records[1][l4];
		newrecords[1][4] = records[1][l5];
		
		newrecords[2][0] = records[2][l1];
		newrecords[2][1] = records[2][l2];
		newrecords[2][2] = records[2][l3];
		newrecords[2][3] = records[2][l4];
		newrecords[2][4] = records[2][l5];
		
		}
		
		if(u == 1 && tick == false){
			String[] art = {records[1][0], records[1][1], records[1][2], records[1][3], records[1][4]};
			String[] preArt = {records[1][0], records[1][1], records[1][2], records[1][3], records[1][4]};
			Arrays.sort(art);

			
			
			//art1
			if(art[0] == preArt[0]){
				l1 = 0;
			}
			if(art[0] == preArt[1]){
				l1 = 1;
			}
			if(art[0] == preArt[2]){
				l1 = 2;
			}
			if(art[0] == preArt[3]){
				l1 = 3;
			}
			if(art[0] == preArt[4]){
				l1 = 4;
			}
			
			
			
			//art2
			if(art[1] == preArt[0]){
				l2 = 0;
			}
			if(art[1] == preArt[1]){
				l2 = 1;
			}
			if(art[1] == preArt[2]){
				l2 = 2;
			}
			if(art[1] == preArt[3]){
				l2 = 3;
			}
			if(art[1] == preArt[4]){
				l2 = 4;
			}
			
			//art3
			if(art[2] == preArt[0]){
				l3 = 0;
			}
			if(art[2] == preArt[1]){
				l3 = 1;
			}
			if(art[2] == preArt[2]){
				l3 = 2;
			}
			if(art[2] == preArt[3]){
				l3 = 3;
			}
			if(art[2] == preArt[4]){
				l3 = 4;
			}			



			//art4
			if(art[3] == preArt[0]){
				l4 = 0;
			}
			if(art[3] == preArt[1]){
				l4 = 1;
			}
			if(art[3] == preArt[2]){
				l4 = 2;
			}
			if(art[3] == preArt[3]){
				l4 = 3;
			}
			if(art[3] == preArt[4]){
				l4 = 4;
			}			


			//art5
			if(art[4] == preArt[0]){
				l5 = 0;
			}
			if(art[4] == preArt[1]){
				l5 = 1;
			}
			if(art[4] == preArt[2]){
				l5 = 2;
			}
			if(art[4] == preArt[3]){
				l5 = 3;
			}
			if(art[4] == preArt[4]){
				l5 = 4;
			}
			
		//restructuring
		newrecords[0][0] = records[0][l1];
		newrecords[0][1] = records[0][l2];
		newrecords[0][2] = records[0][l3];
		newrecords[0][3] = records[0][l4];
		newrecords[0][4] = records[0][l5];
		
		newrecords[1][0] = records[1][l1];
		newrecords[1][1] = records[1][l2];
		newrecords[1][2] = records[1][l3];
		newrecords[1][3] = records[1][l4];
		newrecords[1][4] = records[1][l5];
		
		newrecords[2][0] = records[2][l1];
		newrecords[2][1] = records[2][l2];
		newrecords[2][2] = records[2][l3];
		newrecords[2][3] = records[2][l4];
		newrecords[2][4] = records[2][l5];
		
		}
		
		if(u == 2 && tick == false){
			String[] time = {records[2][0], records[2][1], records[2][2], records[2][3], records[2][4]};
			String[] preTime = {records[2][0], records[2][1], records[2][2], records[2][3], records[2][4]};
			Arrays.sort(time);

			
			
			//time1
			if(time[0] == preTime[0]){
				l1 = 0;
			}
			if(time[0] == preTime[1]){
				l1 = 1;
			}
			if(time[0] == preTime[2]){
				l1 = 2;
			}
			if(time[0] == preTime[3]){
				l1 = 3;
			}
			if(time[0] == preTime[4]){
				l1 = 4;
			}
			
			
			
			//time2
			if(time[1] == preTime[0]){
				l2 = 0;
			}
			if(time[1] == preTime[1]){
				l2 = 1;
			}
			if(time[1] == preTime[2]){
				l2 = 2;
			}
			if(time[1] == preTime[3]){
				l2 = 3;
			}
			if(time[1] == preTime[4]){
				l2 = 4;
			}
			
			//time3
			if(time[2] == preTime[0]){
				l3 = 0;
			}
			if(time[2] == preTime[1]){
				l3 = 1;
			}
			if(time[2] == preTime[2]){
				l3 = 2;
			}
			if(time[2] == preTime[3]){
				l3 = 3;
			}
			if(time[2] == preTime[4]){
				l3 = 4;
			}			



			//time4
			if(time[3] == preTime[0]){
				l4 = 0;
			}
			if(time[3] == preTime[1]){
				l4 = 1;
			}
			if(time[3] == preTime[2]){
				l4 = 2;
			}
			if(time[3] == preTime[3]){
				l4 = 3;
			}
			if(time[3] == preTime[4]){
				l4 = 4;
			}			


			//time5
			if(time[4] == preTime[0]){
				l5 = 0;
			}
			if(time[4] == preTime[1]){
				l5 = 1;
			}
			if(time[4] == preTime[2]){
				l5 = 2;
			}
			if(time[4] == preTime[3]){
				l5 = 3;
			}
			if(time[4] == preTime[4]){
				l5 = 4;
			}
			
		//restructuring
		newrecords[0][0] = records[0][l1];
		newrecords[0][1] = records[0][l2];
		newrecords[0][2] = records[0][l3];
		newrecords[0][3] = records[0][l4];
		newrecords[0][4] = records[0][l5];
		
		newrecords[1][0] = records[1][l1];
		newrecords[1][1] = records[1][l2];
		newrecords[1][2] = records[1][l3];
		newrecords[1][3] = records[1][l4];
		newrecords[1][4] = records[1][l5];
		
		newrecords[2][0] = records[2][l1];
		newrecords[2][1] = records[2][l2];
		newrecords[2][2] = records[2][l3];
		newrecords[2][3] = records[2][l4];
		newrecords[2][4] = records[2][l5];
		
		}
		if(u >= 3 || u <= -1){
			System.out.println("out of reach error! Please Restart");
		}
		System.out.println("DEBUG:");
		System.out.println("presort:");
		System.out.println(records[0][0] + " " + records[1][0] + " " + records[2][0] + "\n" +
						   records[0][1] + " " + records[1][1] + " " + records[2][1] + "\n" +
						   records[0][2] + " " + records[1][2] + " " + records[2][2] + "\n" +
						   records[0][3] + " " + records[1][3] + " " + records[2][3] + "\n"	+  
						   records[0][4] + " " + records[1][4] + " " + records[2][4] + "\n");
		

		System.out.println("postsort:");
		System.out.println(newrecords[0][0] + " " + newrecords[1][0] + " " + newrecords[2][0] + "\n" +
						   newrecords[0][1] + " " + newrecords[1][1] + " " + newrecords[2][1] + "\n" +
						   newrecords[0][2] + " " + newrecords[1][2] + " " + newrecords[2][2] + "\n" +
						   newrecords[0][3] + " " + newrecords[1][3] + " " + newrecords[2][3] + "\n"	+  
						   newrecords[0][4] + " " + newrecords[1][4] + " " + newrecords[2][4] + "\n");
		
		JOptionPane.showMessageDialog(null, "1: " + Recording3B.display(newrecords[0][0], newrecords[1][0], newrecords[2][0]) + "\n2: " + Recording3B.display(newrecords[0][1], newrecords[1][1], newrecords[2][1]) + "\n3: " + Recording3B.display(newrecords[0][2], newrecords[1][2], newrecords[2][2])+ "\n4: " + Recording3B.display(newrecords[0][3], newrecords[1][3], newrecords[2][3]) + "\n5: " + Recording3B.display(newrecords[0][4], newrecords[1][4], newrecords[2][4]), "Sorted", JOptionPane.INFORMATION_MESSAGE);
		
		
	}

}
